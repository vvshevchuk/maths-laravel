FROM php:7.4-fpm-alpine

RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories
RUN apk --no-cache add shadow

RUN apk update && \
    apk add --no-cache \
        $PHPIZE_DEPS \
        nginx \
        git \
        openssh-client \
        openssl-dev \
        zlib-dev \
        libzip-dev \
        curl \
        yarn \
        bash \
        vim

RUN pecl install redis

RUN docker-php-ext-enable opcache
RUN docker-php-ext-enable redis
RUN docker-php-ext-install pdo_mysql zip pcntl

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0"
COPY ./conf/php/opcache.ini "$PHP_INI_DIR/conf.d/opcache.ini"

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN mkdir -p /var/www/html
RUN mkdir /var/cache/nginx

RUN  mkdir -p /etc/nginx && \
     mkdir -p /var/www/html && \
     mkdir -p /etc/nginx/sites-available/ && \
     mkdir -p /etc/nginx/sites-enabled/ && \
     mkdir -p /etc/nginx/ssl &&\
     mkdir -p /var/lib/nginx/.cache/yarn

COPY ./conf/nginx/nginx-site.conf /etc/nginx/conf.d/default.conf
COPY ./conf/nginx/nginx.conf /etc/nginx/nginx.conf

RUN mkdir -p /var/www/html/var
RUN usermod -u 1000 nginx \
    && chown -R 1000:0 /var/cache/nginx \
    && chown -R 1000:0 /var/www/html/var \
    && chown -R 1000:0 /var/lib/nginx/.cache/yarn \
    && chmod -R g+w /var/cache/nginx \
    && chmod -R g+w /var/www/html/var \
    && chmod -R g+w+x /var/lib/nginx/.cache/yarn

RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

WORKDIR /var/www/html

STOPSIGNAL SIGTERM
USER 1000

EXPOSE 8080

CMD /bin/bash -c "nginx && php-fpm"