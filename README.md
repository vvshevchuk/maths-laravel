# about-you-test

## HTTP Methods

Import endpoint from docker: `http://localhost:85/api/v1/import`

Export endpoint from docker: `http://localhost:85/api/v1/export/{indentifier}`

## Console Commands

Import from SFTP server command: `php artisan import:sfpt`

Export to external API command: `php artisan export:api`
